package com.example.vpg.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.vpg.R

class sharedpf:Fragment(R.layout.fragment_sharedpf) {

    private lateinit var textView: TextView
    private lateinit var noteEditText: EditText
    private lateinit var buttonAdd: Button
    private lateinit var sharedPreferences: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(view)
        registerlisteners()
        sharedPreferences = requireActivity().getSharedPreferences("MY_APP_P", AppCompatActivity.MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTES","")
        textView.text = text
    }

    private fun init(view:View){
        textView = view.findViewById(R.id.textView)
        noteEditText = view.findViewById(R.id.noteEditText)
        buttonAdd = view.findViewById(R.id.buttonAdd)
    }
    private fun registerlisteners(){
        buttonAdd.setOnClickListener(){
            val note = textView.text.toString()
            val text = noteEditText.text.toString()
            val result = note + "\n" + text
            textView.text = result
            sharedPreferences.edit()
                .putString("NOTES", result)
                .apply()
        }
    }
}