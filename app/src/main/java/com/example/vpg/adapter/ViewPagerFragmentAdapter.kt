package com.example.vpg.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.vpg.MainActivity
import com.example.vpg.fragments.FragmentOne
import com.example.vpg.fragments.FragmentTwo
import com.example.vpg.fragments.sharedpf

class ViewPagerFragmentAdapter(activity: MainActivity):FragmentStateAdapter(activity) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when (position){
            0 -> FragmentOne()
            1 -> FragmentTwo()
            2 -> sharedpf()
            else -> FragmentOne()
        }
    }
}